import React, { Component } from "react";
import { Row } from "react-bootstrap";

export default class MyNavbar extends Component {
  render() {
    return (
      <>
        <Row>
          <div className="col-lg-9">
            <h3>KSHRD Center</h3>
          </div>
          <div className="col-lg-3 ">
            <p
              style={{
                marginTop: "25px",
                fontSize: "15px",
                textAlign: "right",
              }}
            >
              Sign-in:{" "}
              <a
                href="#"
                style={{
                  textDecoration: "none",
                  color: "black",
                  fontWeight: "bold",
                }}
              >
                Lun Timeng
              </a>
            </p>
          </div>
        </Row>
        <hr />
      </>
    );
  }
}
