import React from "react";
import { Table, Button, Col } from "react-bootstrap";

export default function MyTable({ emp, removeElement, getIndex }) {




  return (
    <>
      <h3>Employee List</h3>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>UserName</th>
            <th>Email</th>
            <th>Gender</th>
          </tr>
        </thead>
        <tbody>
            {emp.map((item,index)=>(
              <tr key={index} onClick={()=>getIndex(index)} id="trow">
              <td>{index+1}</td>
              <td>{item.name}</td>
              <td>{item.email}</td>
              <td>{item.gender}</td>
            </tr>
            ))}
        </tbody>
      </Table>
      <Col>
        <Button disabled={emp.length===0} variant="danger" type="button" onClick={removeElement}>
          Delete
        </Button>
      </Col>
    </>
  );
}
