import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container, Row } from "react-bootstrap";
import MyNavbar from "./Compnent/MyNavbar";
import MyForm from "./Compnent/MyForm";
import MyTable from "./Compnent/MyTable";

export default class App extends React.Component {


  constructor(props) {
    super(props);

    this.state = {
      emp: [
          {name:"TIMENG",email:"luntimeng@gmailcom",gender:"male"},{name:"TIM",email:"luntimeng@gmailcom",gender:"male"},{name:"TENG",email:"luntimeng@gmailcom",gender:"male"}
      ],
      name: "",
      email: "",
      password: "",
      gender:"male",
      bool:true,
      arrindex:[],
    };
  }



  getValue = (event) => {

    event.preventDefault();
    //get name email and pass form obj
    const name = event.target.name;
    const value = event.target.value;

    if(document.getElementById('username').value != '' 
    &&  document.getElementById('email').value != '' 
    && document.getElementById('password').value != '' ){
        this.setState({
            bool:false
        })
    }else{
        this.setState({
            bool:true
        })
    }
    this.setState({ [name]: value });
  };


  handleAdd = (event) => {

    //set textfield empty
    document.getElementById('username').value = ''
    document.getElementById('email').value = ''
    document.getElementById('password').value = ''

    const newEmp = [...this.state.emp];

    event.preventDefault();

    const name = this.state.name;
    const email = this.state.email;
    const password = this.state.password;
    const gender = this.state.gender

    //create object for store name,email,password
    const newObj = {
        name:name.toUpperCase(),
        email:email,
        password:password,
        gender:gender.toLowerCase()
    }


    this.setState({
        emp: [...newEmp,newObj],
        bool:true
      });

    
  };

  //change color table row


  getIndex = (index) =>{


    //hightlight table row by click
    const trow = document.getElementsByTagName('tr') [index+1];
    trow.style.backgroundColor = "black"
    trow.style.color = "white"

        this.state.arrindex.push(index)

        this.setState({
            arrindex: this.state.arrindex,
        });

    console.log(index)
    console.log("array: ",this.state.arrindex)
  }

  removeElement = () => {
      
    //reset color on table row
    const trow = document.getElementById('trow');
    trow.style.backgroundColor = "white"
    trow.style.color = "black"

    //check item1 and item2 of arrindex
    for(let i=0;i<this.state.arrindex.length;i++){
        if(this.state.arrindex[i]>this.state.arrindex[i+1]){
            //remove from bottom to top    
            for(let i=0;i<this.state.arrindex.length;i++){
                this.state.emp.splice(this.state.arrindex[i],1)
            }

            break
        }else{
            //remove from top to bottom
            for(let i = this.state.arrindex.length-1; i >=0; i--){
                this.state.emp.splice(this.state.arrindex[i],1)
            }

            break
        }
    }

    //remove index
    for(let i = this.state.arrindex.length-1; i >=0; i--){
        this.state.arrindex.pop(this.state.arrindex[i])
    }

    this.setState({
        emp:this.state.emp,
        arrindex:this.state.arrindex
    })

    console.log("arr ",this.state.arrindex.length)

  }


  render() {
    return (
      <Container>
        <br />
        <Row>
          <div className="col-lg-12">
            <MyNavbar />
          </div>
          <br />
          <br />
          <br />
          <div className="col-lg-4 ">
            <MyForm
              emp={this.state.emp}
              getValue={this.getValue}
              handleAdd={this.handleAdd}
              bool={this.state.bool}
            />
          </div>
          <div className="col-lg-8">
            <MyTable 
            emp={this.state.emp} 
            removeElement = {this.removeElement}
            getIndex={this.getIndex}
            />
          </div>
        </Row>
      </Container>
    );
  }
}
