import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";

export default class MyForm extends Component {


 
  //jsx
  render() {
    return (
      <>
        <h3>Add Employee</h3>
        <Form>
          <Form.Group>
            <Form.Label>Username</Form.Label>
            <Form.Control
              id="username"
              type="text"
              onChange={this.props.getValue}
              placeholder="Username"
              name="name"
            />
          </Form.Group>

          <br />
          <Form.Group>
            <Form.Label>Email address</Form.Label>
            <Form.Control
              id="email"
              type="email"
              onChange={this.props.getValue}
              placeholder="Enter email"
              name="email"
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control
              id="password"
              type="password"
              onChange={this.props.getValue}
              placeholder="Password"
              name="password"
            />
          </Form.Group><br/>

          <Form.Group>
            <Form.Label>Gender</Form.Label>
            <Form.Check
              type="radio"
              label="Male"
              name="gender"
              value="Male"
              checked={true}
              onChange={this.props.getValue}
              id="r1"
            />

            <Form.Check
              type="radio"
              label="Female"
              name="gender"
              value="Female"
              onChange={this.props.getValue}
              id="r2"
            />
          </Form.Group>

          <br />
          <Button
            disabled={this.props.bool}
            variant="primary"
            onClick={this.props.handleAdd}
            // onClick={this.oncheckText}
            type="submit"
          >
            Submit
          </Button>
        </Form>
        <br/>
      </>
    );
  }
}
